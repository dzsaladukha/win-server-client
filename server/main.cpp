#include <iostream>
#include <io.h>
#include <fcntl.h>
#include <thread>
#include <concurrent_queue.h>
#include "servicing.h"
#include "Commandreader.h"

BOOL WINAPI HandlerRoutine(DWORD ctrlType);

volatile bool g_stopFlag = true;

int main()
{
    _setmode(_fileno(stdout), _O_U16TEXT);
    SetConsoleCtrlHandler(HandlerRoutine, TRUE);

    WSAData wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        std::wcerr << L"WSAStartup failed with error " << WSAGetLastError() << std::endl;
        return 1;
    }

    HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
    if (SUCCEEDED(hr))
    {
        CCommandReader commandReader;
        commandReader.LoadXml(L"test1.xml");

        Concurrency::concurrent_queue<CMessage> commandQueue;
        Concurrency::concurrent_queue<CMessage> responseQueue;

        std::thread workerThread([&]() {
            Serve(commandQueue, responseQueue);
            });

        CMessage message;
        while (g_stopFlag)
        {
            if (commandQueue.try_pop(message))
            {
                message.data = move(commandReader.GetResponseTo(message.data));
                responseQueue.push(message);
            }
            else
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
            }
        }

        workerThread.join();
    }

    CoUninitialize();
    WSACleanup();
    return 0;
}

BOOL WINAPI HandlerRoutine(DWORD ctrlType)
{
    switch (ctrlType)
    {
    case CTRL_C_EVENT:
        g_stopFlag = false;
        return TRUE;
    default:
        return FALSE;
    }
}
