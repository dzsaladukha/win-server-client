#pragma once
#include <vector>
#include <string>

#import <msxml6.dll>

class CCommandReader
{
public:
    HRESULT LoadXml(const WCHAR* filename);
    std::vector<std::wstring> GetCommands();
    std::wstring GetResponseTo(const std::wstring& command);

private:
    MSXML2::IXMLDOMDocumentPtr m_pXmlDom;
};

