#include "servicing.h"
#include <iostream>
#include <vector>

#include "timestamp.h"

extern volatile bool g_stopFlag;

void Serve(Concurrency::concurrent_queue<CMessage>& commandQueue, Concurrency::concurrent_queue<CMessage>& responseQueue)
{
    SOCKET listenSocket = INVALID_SOCKET;
    SOCKET acceptedSocket = INVALID_SOCKET;
    const int connectionLimit = 10;
    std::vector<SOCKET> sockets(connectionLimit, INVALID_SOCKET);
    sockaddr_in selfAddress;
    const int bufferSize = 512;
    char buffer[bufferSize] { 0 };
    FD_SET readSet;
    int selectResult;
    int result;
    CMessage message;

    selfAddress.sin_family = AF_INET;
    selfAddress.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    selfAddress.sin_port = htons(3333);

    listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (INVALID_SOCKET == listenSocket)
    {
        std::wcerr << L"Failed to create listen socket." << std::endl;
        return;
    }

    result = bind(listenSocket, reinterpret_cast<struct sockaddr*>(&selfAddress), sizeof(selfAddress));
    if (SOCKET_ERROR == result)
    {
        std::wcerr << L"Bind failed with error " << WSAGetLastError() << std::endl;
        closesocket(listenSocket);
        return;
    }

    if (listen(listenSocket, connectionLimit))
    {
        std::wcerr << L"Listen failed with error " << WSAGetLastError() << std::endl;
        return;
    }
    else
    {
        std::wcout << L"Start listening..." << std::endl;
    }

    TIMEVAL timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 100000; // 100ms

    while (g_stopFlag)
    {
        FD_ZERO(&readSet);
        FD_SET(listenSocket, &readSet);

        for (int i = 0; i < connectionLimit; i++)
        {
            if (sockets[i] != INVALID_SOCKET)
                FD_SET(sockets[i], &readSet);
        }

        selectResult = select(0, &readSet, NULL, NULL, &timeout);
        if (selectResult == SOCKET_ERROR)
        {
            std::wcerr << L"Select have returned with error " << WSAGetLastError() << std::endl;
            continue;
        }

        if (selectResult > 0)
        {
            if (FD_ISSET(listenSocket, &readSet))
            {
                acceptedSocket = accept(listenSocket, nullptr, nullptr);
                if (acceptedSocket != INVALID_SOCKET)
                {
                    for (int i = 0; i < connectionLimit; i++)
                    {
                        if (INVALID_SOCKET == sockets[i])
                        {
                            sockets[i] = acceptedSocket;
                            std::wcout << Timestamp() << L" Client #" << i << L" Connection accepted.\n";
                            break;
                        }
                    }
                }
                else
                {
                    std::wcerr << Timestamp() << L" Accept failed with error " << WSAGetLastError() << std::endl;
                }
            }

            for (int i = 0; i < connectionLimit; i++)
            {
                if (sockets[i] != INVALID_SOCKET && FD_ISSET(sockets[i], &readSet))
                {
                    result = recv(sockets[i], buffer, bufferSize, 0);
                    if (result > 0)
                    {
                        std::wstring receivedData(reinterpret_cast<const WCHAR*>(buffer), result / 2);
                        std::wcout << Timestamp() << L" Client #" << i << L" Received command: " << receivedData << '\n';

                        message.index = i;
                        message.data = move(receivedData);
                        commandQueue.push(message);
                    }
                    else
                    {
                        if (0 == result)
                        {
                            std::wcout << Timestamp() << L" Client #" << i << L" Connection has been gracefully closed." << std::endl;
                        }
                        else
                        {
                            std::wcerr << L"recv failed with error " << WSAGetLastError() << std::endl;
                            std::wcout << L"Closing connection with Client #" << i << std::endl;
                        }

                        closesocket(sockets[i]);
                        sockets[i] = INVALID_SOCKET;
                    }
                }
            }
        }

        while (responseQueue.try_pop(message))
        {
            if (sockets[message.index] != INVALID_SOCKET)
            {
                result = send(sockets[message.index], reinterpret_cast<const char*>(message.data.c_str()),
                    (int)message.data.size() * sizeof(std::wstring::value_type), 0);

                if (result > 0)
                {
                    std::wcout << Timestamp() << L" Client #" << message.index << L" Sent response: " << message.data << '\n';
                }
                else
                {
                    std::wcerr << L"send failed with error " << WSAGetLastError() << std::endl;
                }
            }
            else
            {
                std::wcerr << L"Response to INVALID connection." << std::endl;
            }
        }
    }

    for (int i = 0; i < connectionLimit; i++)
    {
        if (sockets[i] != INVALID_SOCKET)
        {
            std::wcout << Timestamp() << L" Client #" << i << L" Closing connection.\n";
            closesocket(sockets[i]);
        }
    }

    closesocket(listenSocket);
}
