#include "timestamp.h"
#include <chrono>
#include <iomanip>
#include <sstream>

std::wstring Timestamp()
{
    auto now = std::chrono::system_clock::now();
    auto nowInMs = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;
    time_t timeValue = std::chrono::system_clock::to_time_t(now);
    tm localTime;
    localtime_s(&localTime, &timeValue);
    std::wstringstream ss;
    ss << std::put_time(&localTime, L"%T.") << std::setfill(L'0') << std::setw(3) << nowInMs.count();
    return ss.str();
}
