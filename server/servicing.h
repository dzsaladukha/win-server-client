#pragma once
#include <concurrent_queue.h>
#include <string>
#include <WinSock2.h>

#pragma comment (lib, "Ws2_32.lib")

struct CMessage
{
    int index = 0;
    std::wstring data;
};

void Serve(Concurrency::concurrent_queue<CMessage>& commandQueue, Concurrency::concurrent_queue<CMessage>& responseQueue);
