#include "CommandReader.h"
#include <iostream>

HRESULT CCommandReader::LoadXml(const WCHAR* filename)
{
    HRESULT hr;

    hr = m_pXmlDom.CreateInstance(__uuidof(MSXML2::DOMDocument60), nullptr, CLSCTX_INPROC_SERVER);

    if (SUCCEEDED(hr))
    {
        try
        {
            if (m_pXmlDom->load(filename) == VARIANT_TRUE)
            {
                std::wcout << L"XML DOM loaded from " << filename << std::endl;
                hr = S_OK;
            }
            else
            {
                std::wcout << L"Failed to load DOM from " << filename << ". " << m_pXmlDom->parseError->Getreason() << std::endl;
                hr = E_FAIL;
            }
        }
        catch (const _com_error& ex)
        {
            std::wcerr << L"Exception thrown while loading XML, HRESULT: 0x" << std::hex << ex.Error() << std::endl;
            hr = E_FAIL;
        }
    }
    else
    {
        std::wcerr << L"Failed to instantiate XML document." << std::endl;
    }

    return hr;
}

std::vector<std::wstring> CCommandReader::GetCommands()
{
    std::vector<std::wstring> commands;

    if (m_pXmlDom)
    {
        try
        {
            MSXML2::IXMLDOMNodeListPtr pNodeList = m_pXmlDom->selectNodes(L"//command");
            if (pNodeList)
            {
                for (long i = 0; i < pNodeList->Getlength(); i++)
                {
                    commands.push_back(std::wstring(pNodeList->Getitem(i)->GetfirstChild()->Gettext()));
                }
            }
            else
            {
                std::wcout << L"No commands found in xml." << std::endl;
            }
        }
        catch (const _com_error& ex)
        {
            std::wcerr << L"Exception thrown while reading XML, HRESULT: 0x" << std::hex << ex.Error() << std::endl;
        }
    }

    return commands;
}

std::wstring CCommandReader::GetResponseTo(const std::wstring& command)
{
    MSXML2::IXMLDOMNodePtr pNode;

    if (m_pXmlDom)
    {
        try
        {
            std::wstring query = L"//command[normalize-space(text()[1]) = '" + command + L"']/response";
            pNode = m_pXmlDom->selectSingleNode(query.c_str());
        }
        catch (const _com_error& ex)
        {
            std::wcerr << L"Exception thrown while querying XML, HRESULT: 0x" << std::hex << ex.Error() << std::endl;
        }
    }

    return pNode ? std::wstring(pNode->Gettext()) : L"Unknown Command";
}
