#pragma once
#include <vector>
#include <string>
#include <WinSock2.h>
#include <ws2tcpip.h>

#pragma comment (lib, "Ws2_32.lib")

void DoChatter(const WCHAR* address, USHORT port, const std::vector<std::wstring>& commands, int index);
