﻿#include <iostream>
#include <io.h>
#include <fcntl.h>
#include <vector>
#include <thread>
#include "chatting.h"
#include "Commandreader.h"

BOOL WINAPI HandlerRoutine(DWORD dwCtrlType);

volatile bool g_stopFlag = true;

int main()
{
    _setmode(_fileno(stdout), _O_U16TEXT);
    SetConsoleCtrlHandler(HandlerRoutine, TRUE);

    HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
    if (FAILED(hr))
    {
        std::wcerr << L"Failed to CoInitialize msxml6" << std::endl;
        return 1;
    }

    const std::vector<std::wstring> commands = std::move([]() {
        CCommandReader commandReader;
        commandReader.LoadXml(L"test1.xml");
        return commandReader.GetCommands();
        }());

    CoUninitialize();

    std::wcout << L"Commands read: " << commands.size() << std::endl;

    WSAData wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        std::wcout << L"WSAStartup failed with error " << WSAGetLastError() << std::endl;
        return 1;
    }

    const int threadNumber = 10;
    std::vector<std::thread> threads(threadNumber);
    for (int i = 0; i < threadNumber; i++)
    {
        threads[i] = std::thread(DoChatter, L"127.0.0.1", 3333, commands, i);
        std::this_thread::sleep_for(std::chrono::milliseconds(400));
    }

    std::wcin.get();
    g_stopFlag = false;

    for (int i = 0; i < threadNumber; i++)
    {
        if (threads[i].joinable())
        {
            threads[i].join();
        }
    }

    WSACleanup();
    return 0;
}

BOOL WINAPI HandlerRoutine(DWORD ctrlType)
{
    switch (ctrlType)
    {
    case CTRL_C_EVENT:
        g_stopFlag = false;
        return TRUE;
    default:
        return FALSE;
    }
}
