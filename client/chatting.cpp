#include "chatting.h"
#include <iostream>
#include <thread>
#include "timestamp.h"

extern volatile bool g_stopFlag;

void DoChatter(const WCHAR* address, USHORT port, const std::vector<std::wstring>& commands, int index)
{
    SOCKET clientSocket;
    sockaddr_in destination;
    int result;

    clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (clientSocket == INVALID_SOCKET)
    {
        std::wcerr << L"Error creating socket " << WSAGetLastError() << std::endl;
    }

    destination.sin_family = AF_INET;
    result = InetPtonW(AF_INET, address, &destination.sin_addr.s_addr);
    if (result != 1)
    {
        std::wcerr << L"Incorrect address" << std::endl;
    }
    destination.sin_port = htons(port);

    while (g_stopFlag && connect(clientSocket, reinterpret_cast<struct sockaddr*>(&destination), sizeof(destination)) != 0)
    {
        std::wcerr << L"Connection error " << WSAGetLastError() << std::endl;
    }

    int commandsSize = static_cast<int>(commands.size());
    const int bufferSize = 512;
    char buffer[bufferSize] { 0 };

    std::chrono::duration<double, std::milli> error;
    auto period = std::chrono::seconds(1);
    auto target = std::chrono::system_clock::now();
    while (g_stopFlag)
    {
        for (int i = 0; i < commandsSize && g_stopFlag; i++)
        {
            result = send(clientSocket, reinterpret_cast<const char*>(commands[i].c_str()),
                static_cast<int>(commands[i].size()) * sizeof(std::wstring::value_type), 0);
            if (result > 0)
            {
                std::wcout << Timestamp() + L" Thread #" + std::to_wstring(index) + L" Sent command: " + commands[i] + L"\n";
            }

            result = recv(clientSocket, buffer, bufferSize, 0);
            if (result > 0)
            {
                std::wstring ws(reinterpret_cast<const WCHAR*>(buffer), result / 2);
                std::wcout << Timestamp() + L" Thread #" + std::to_wstring(index) + L" Received text: " + ws + L"\n";
            }
            else if (0 == result)
            {
                std::wcout << Timestamp() + L" Thread #" + std::to_wstring(index) + L" Connection closed.\n";
                closesocket(clientSocket);
                return;
            }
            else
            {
                std::wcerr << Timestamp() + L" Thread #" + std::to_wstring(index) + L" recv failed with error: " + std::to_wstring(WSAGetLastError()) + L"\n";
                closesocket(clientSocket);
                return;
            }

            target += period;
            error = target - std::chrono::system_clock::now();
            std::this_thread::sleep_for(error);
        }
    }

    closesocket(clientSocket);
}
